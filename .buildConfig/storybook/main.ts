
import {Configuration} from 'webpack';
import path from 'path';


export default {
    
    stories: ['../../test/stories/**/*.(tsx|ts)'],
    
    addons: [
        '@storybook/addon-viewport',
        '@storybook/addon-actions'
    ],

    webpackFinal: async (config: Configuration) => {

        return {
            ...config,

            resolve: {
                extensions: ['.ts', '.tsx', '.js', '.jsx']
            },

            module: {
                ...config.module,
                rules: [
                    {
                        test: /\.js$/,
                        exclude: path.resolve(__dirname, '..', '..', 'node_modules'),
                        loader: 'babel-loader'
                    },
                    {
                        test: /\.(ts|tsx)$/,
                        exclude: path.resolve(__dirname, '..', '..', 'node_modules'),
                        include: [
                            path.resolve(__dirname, '..', '..', 'src'),
                            path.resolve(__dirname, '..', '..', 'test', 'stories')
                        ],
                        loader: 'babel-loader'
                    },
                    {
                        test: /\.scss$/,
                        exclude: path.resolve(__dirname, '..', '..', 'node_modules'),
                        use: [
                            'style-loader',
                            {
                                loader: 'css-loader',
                                options: {
                                    modules: {
                                        localIdentName: "[name]-[local]"
                                    },
                                    importLoaders: 2,
                                    localsConvention: 'camelCase',
                                    sourceMap: true
                                }
                            },
                            'sass-loader',
                            {
                                loader: 'sass-resources-loader',
                                options: {
                                    resources: path.resolve(__dirname, '..', '..', 'src', 'styles', '_variables.scss')
                                }
                            }
                        ]
                    }
                ]
            }
        };
    }
};
