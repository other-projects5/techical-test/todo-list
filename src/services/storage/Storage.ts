
import {IAppState} from '../../store/IAppState';


export class Storage {

    private readonly KEY: string = 'state';

    public constructor() {}


    public getStateFromStorage(): any | undefined {

        try {

            const serializedState = localStorage.getItem(this.KEY);

            return serializedState === null ? undefined : this.prepareState(JSON.parse(serializedState));

        } catch (err) {
            return undefined;
        }
    }

    public saveStateToStorage(state: IAppState): void {

        try {

            const serializedState = JSON.stringify(state);

            localStorage.setItem(this.KEY, serializedState);

        } catch (err) {
            console.log('ERROR', err);
        }
    }

    private prepareState(state: any): IAppState {

        return {
            ...state,
            card: {
                ...state.card,
                cards: state.card.cards.map((c: any) => ({...c, createDate: new Date(c.createDate)}))
            }
        }
    }
}
