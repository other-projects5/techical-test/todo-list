
import {ICard} from '../model';


function getOrdered<T>(oldValue: T, newValue: T, orderAsc: boolean): number {

    if (oldValue < newValue) {
        return orderAsc ? 1 : -1;

    } else if (oldValue > newValue) {
        return orderAsc ? -1 : 1;

    } else {
        return 0;
    }
}

export function orderByTitle(oldValue: ICard, newValue: ICard, type: boolean): number {
    return getOrdered(oldValue.title, newValue.title, type);
}

export function orderByDate(oldValue: ICard, newValue: ICard, type: boolean): number {
    return getOrdered(oldValue.createDate, newValue.createDate, type);
}
