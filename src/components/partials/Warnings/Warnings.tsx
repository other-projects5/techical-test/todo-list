
import * as React from 'react';
import {FunctionComponent} from 'react';

import {Alert} from '@material-ui/lab';

import {IWarning} from '../../../model';


const styles = require('./Warnings.scss');

interface WarningsProps extends IWarning {}

export const Warnings: FunctionComponent<WarningsProps> = (props) => {

    const {type, message} = props;


    return type === null ? null : (
        <div className={styles.warningWrapper} >
            <Alert variant="filled"
                   severity={type}
                   className={styles.warning} >

                {message}

            </Alert>
        </div>
    );
};
