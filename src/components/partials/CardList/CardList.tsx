
import * as React from 'react';
import {FunctionComponent, useState} from 'react';
import {useSelector} from 'react-redux';

import {Grid, Switch, RadioGroup, FormControlLabel, Radio} from '@material-ui/core';

import {IAppState} from '../../../store/IAppState';

import {orderByDate, orderByTitle} from '../../../utils';
import {ICard} from '../../../model';
import {Card} from './Card';


const styles = require('./CardList.scss');

declare type RadioBtnType = 'title' | 'date';

export const CardList: FunctionComponent = () => {

    const {cards} = useSelector((state: IAppState) => state.card);

    const [orderAsc, setOrderAsc] = useState<boolean>(false);
    const [radioVal, setRadioVal] = useState<RadioBtnType>('title');


    const orderByFilters = (array: ICard[]): ICard[] => {

        return array.sort((oldValue, newValue) => {
            return radioVal === 'title' ?
                orderByTitle(oldValue, newValue, orderAsc)
                : orderByDate(oldValue, newValue, orderAsc)
        });
    }

    return (
        <div>

            <div className={styles.filterWrapper} >
                <RadioGroup name="filters"
                            value={radioVal}
                            onChange={(event) => setRadioVal(event.target.value as RadioBtnType)} >

                    <FormControlLabel value="title" control={<Radio />} label="Título" />
                    <FormControlLabel value="date" control={<Radio />} label="Fecha" />
                </RadioGroup>

                <div className={styles.switchWrapper} >
                    <span className={styles.label} >
                        Descendente
                    </span>

                    <Switch color="secondary"
                            checked={orderAsc}
                            onChange={() => setOrderAsc(!orderAsc)}
                            name="title" />

                    <span className={styles.label} >
                        Ascendente
                    </span>
                </div>
            </div>

            <Grid container spacing={3} >

                {orderByFilters(cards).map((c) => (
                    <Grid key={c.id}
                          className={styles.column}
                          item
                          xs={12} sm={6} md={4} lg={3} >

                        <Card {...c} />

                    </Grid>
                ))}

            </Grid>

        </div>
    );
};
