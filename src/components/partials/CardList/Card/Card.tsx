
import * as React from 'react';
import {FunctionComponent} from 'react';
import {useDispatch} from 'react-redux';

import {Card as MuiCard, Button} from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';

import {Dispatch} from '../../../../store/types/redux';
import {deleteCard, updateCurrentCard} from '../../../../store/actions/cardActions';

import {ICard} from '../../../../model';


const styles = require('./Card.scss');

interface CardProps extends ICard {}

export const Card: FunctionComponent<CardProps> = (props) => {

    const DEFAULT_IMAGE = 'https://kubalubra.is/wp-content/uploads/2017/11/default-thumbnail.jpg';

    const dispatch: Dispatch = useDispatch();
    const deleteCardDispatch = (id: string) => dispatch(deleteCard(id));
    const updateCurrentCardDispatch = (id: string) => dispatch(updateCurrentCard(id));

    const {id, createDate, title, description, image} = props;


    const onEditClick = (): void => {
        updateCurrentCardDispatch(id);
    }

    const onDeleteClick = (): void => {
        deleteCardDispatch(id);
    }


    return (
        <MuiCard elevation={8} className={styles.card} >

            <div className={styles.imageWrapper} >
                <div className={styles.actionButtonWrapper} >
                    <Button color="secondary" className={styles.actionButton} onClick={onEditClick} >
                        <EditIcon />
                    </Button>
                    <Button className={styles.actionButton} onClick={onDeleteClick} >
                        <DeleteIcon />
                    </Button>
                </div>

                <img src={image || DEFAULT_IMAGE} alt="image" />

                <div className={styles.title} >
                    {title}
                </div>
            </div>

            <div className={styles.content} >
                <div className={styles.description} >
                    {createDate.toLocaleDateString()}: {description}
                </div>
            </div>

        </MuiCard>
    );
};
