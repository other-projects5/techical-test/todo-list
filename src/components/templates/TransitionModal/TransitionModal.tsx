
import * as React from 'react';
import {FunctionComponent} from 'react';

import {Backdrop, Fade, Modal} from '@material-ui/core';


const styles = require('./TransitionModal.scss');

interface TransitionModalProps {

    visible: boolean;
    transitionTimeout?: number;
    onClose(): void;

}

export const TransitionModal: FunctionComponent<TransitionModalProps> = (props) => {

    const {children, visible, transitionTimeout = 500, onClose} = props;


    return (
        <Modal aria-labelledby="transition-modal-title"
               aria-describedby="transition-modal-description"
               open={visible}
               onClose={onClose}
               className={styles.modal}
               closeAfterTransition
               BackdropComponent={Backdrop}
               BackdropProps={{timeout: transitionTimeout}} >

            <Fade in={visible} >
                <div className={styles.container} >

                    {children}

                </div>
            </Fade>

        </Modal>
    );
};
