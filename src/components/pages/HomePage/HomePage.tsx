
import * as React from 'react';
import {FunctionComponent, useEffect, useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';

import {Container, Fab} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';

import {Dispatch} from '../../../store/types/redux';
import {IAppState} from '../../../store/IAppState';
import {updateCurrentCard} from '../../../store/actions/cardActions';

import {CardFormModal} from '../../modals';
import {CardList} from '../../partials';


const styles = require('./HomePage.scss');

export const HomePage: FunctionComponent = () => {

    const dispatch: Dispatch = useDispatch();
    const updateCurrentCardDispatch = () => dispatch(updateCurrentCard());

    const [modalVisible, setModalVisible] = useState<boolean>(false);

    const {updateCurrent} = useSelector((state: IAppState) => state.card);


    useEffect(() => {
        if(updateCurrent !== '') {
            setModalVisible(true);
        }
    }, [updateCurrent]);


    const onClose = () => {

        updateCurrentCardDispatch();
        setModalVisible(false);
    }


    return (
        <Container className={styles.container} >

            <CardList />

            <Fab color="primary" className={styles.button} onClick={() => setModalVisible(true)} >
                <AddIcon />
            </Fab>

            <CardFormModal visible={modalVisible} onClose={onClose} />

        </Container>
    );
}
