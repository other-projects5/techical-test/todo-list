
import * as React from 'react';
import {FunctionComponent} from 'react';

import {createMuiTheme, Theme} from '@material-ui/core';
import {red, orange} from '@material-ui/core/colors';
import {ThemeProvider} from '@material-ui/styles';


export const MuiThemeProvider: FunctionComponent = (props) => {

    const theme: Theme = createMuiTheme({
        palette: {
            background: {
                paper: '#fafafa',
                default: '#ccc'
            },
            primary: {
                main: red[500],
                contrastText: 'white'
            },
            secondary: {
                main: orange[500],
                contrastText: 'white'
            }
        },
        overrides: {
            MuiGrid: {
                container: {
                    margin: '0 !important',
                    width: '100% !important'
                }
            }
        },
        props: {
            MuiButton: {
                variant: 'contained',
                color: 'primary'
            }
        }
    });


    return (
        <ThemeProvider theme={theme} >

            {props.children}

        </ThemeProvider>
    );
};
