
import * as React from 'react';
import {FunctionComponent} from 'react';

import {TransitionModal} from '../../templates';
import {CardForm} from './CardForm';


const styles = require('./CardFormModal.scss');

interface CardFormModalProps {

    visible: boolean;
    onClose(): void;

}

export const CardFormModal: FunctionComponent<CardFormModalProps> = (props) => {

    const {visible, onClose} = props;


    return (
        <TransitionModal visible={visible} onClose={onClose} >

            <div>
                <h1 className={styles.title} >
                    Nueva tarjeta
                </h1>

                <CardForm onSubmit={onClose} />
            </div>

        </TransitionModal>
    );
};
