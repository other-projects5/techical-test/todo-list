
import * as React from 'react';
import {ChangeEvent, FunctionComponent, useEffect, useState} from 'react';
import {useDispatch, useSelector} from 'react-redux';
import {v4 as uuidv4} from 'uuid';

import {Button, Input} from '@material-ui/core';

import {Dispatch} from '../../../../store/types/redux';
import {createUpdateCard} from '../../../../store/actions/cardActions';

import {ICard, IWarning} from '../../../../model';
import {Warnings} from '../../../partials';
import {IAppState} from '../../../../store/IAppState';


const styles = require('./CardForm.scss');

interface CardFormProps {

    onSubmit(): void;

}

export const CardForm: FunctionComponent<CardFormProps> = (props) => {

    const {cards, updateCurrent} = useSelector((state: IAppState) => state.card);

    const dispatch: Dispatch = useDispatch();
    const createUpdateCardDispatch = (form: ICard) => dispatch(createUpdateCard(form));

    const initialState = {id: uuidv4(), createDate: new Date(), title: '', description: '', image: ''}
    const [form, setForm] = useState<ICard>(initialState);
    const [warning, setWarning] = useState<IWarning>({type: null, message: ''});


    useEffect(() => {

        const currentCard = cards.find(c => c.id === updateCurrent);

        if(currentCard) {
            setForm(currentCard);
        }

    }, [updateCurrent]);

    useEffect(() => {

        const timer = setTimeout(() => {
            setWarning({type: null, message: ''});
        }, 2000);

        return () => clearTimeout(timer);
    }, [warning]);


    const onInputChange = (event: ChangeEvent<HTMLInputElement>): void => {

        const {name, value} = event.currentTarget;

        setForm({
            ...form,
            [name]: value
        });
    };

    const onButtonClick = (): void => {

        if(!form.title || !form.description) {

            setWarning({type:'error', message: 'Los campos con (*) son necesarios'});
            return;
        }

        const data: ICard = {...form};

        !data.image && delete data.image;

        createUpdateCardDispatch(data);

        setForm(initialState);

        props.onSubmit();
    }


    return (
        <div className={styles.formWrapper} >
            <Input type="text"
                   name="title"
                   placeholder="Título *"
                   value={form.title}
                   onChange={onInputChange}
                   required />

            <Input type="text"
                   name="description"
                   placeholder="Descripción *"
                   value={form.description}
                   onChange={onInputChange}
                   required />

            <Input type="text"
                   name="image"
                   placeholder="Imagen (URL)"
                   value={form.image}
                   onChange={onInputChange} />

            <div className={styles.buttonWrapper} >
                <Button onClick={onButtonClick} >
                    Añadir
                </Button>
            </div>

            <Warnings {...warning} />

        </div>
    );
};
