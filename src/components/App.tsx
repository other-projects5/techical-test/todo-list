
import * as React from 'react';
import {FunctionComponent} from 'react';
import {BrowserRouter} from 'react-router-dom';
import {Provider} from 'react-redux';

import {CssBaseline} from '@material-ui/core';

import {store} from '../store/store';
import {Routes} from './routes';
import {MuiThemeProvider} from './MuiThemeProvider';


export const App: FunctionComponent = () => {

    return (
        <MuiThemeProvider>

            <CssBaseline />

            <Provider store={store} >

                <BrowserRouter>
                    <Routes />
                </BrowserRouter>

            </Provider>

        </MuiThemeProvider>
    );
}
