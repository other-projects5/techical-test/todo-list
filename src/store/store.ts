
import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import {Storage} from '../../src/services';

import {rootReducer} from './reducers/rootReducer'
import {initialState} from './initialState';


const storageService = new Storage();

const state = storageService.getStateFromStorage() || initialState;

const middleware = [thunk];

export const store = createStore(
    rootReducer,
    state,
    composeWithDevTools(applyMiddleware(...middleware))
);


store.subscribe(() => {
    storageService.saveStateToStorage(store.getState());
});
