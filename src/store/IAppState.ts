
import {ICard} from "../model";


export interface IAppState {

    card: ICardState;

}

export interface ICardState {

    cards: ICard[];

    updateCurrent: string;

}
