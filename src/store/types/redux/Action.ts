
import {ThunkAction} from 'redux-thunk';
import {AnyAction} from 'redux';


export type Action = ThunkAction<Promise<void> | void, {}, {}, AnyAction>;
