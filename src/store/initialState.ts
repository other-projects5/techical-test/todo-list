
import {IAppState} from './IAppState';


const state: IAppState = {

    card: {
        cards: [],
        updateCurrent: ''
    }

};


export const initialState: IAppState = process.env.NODE_ENV === 'development' ? {
    ...state,
    card: {
        ...state.card,
        cards: [{
            id: '1',
            createDate: new Date(2020, 10, 1),
            title: 'Futbol',
            description: 'Es un deporte de equipo jugado entre dos conjuntos de once jugadores cada uno y algunos árbitros que se ocupan de que las normas se cumplan correctamente.',
            image: 'https://images.freeimages.com/images/large-previews/0d2/soccer-2-1430279.jpg'
        }, {
            id: '2',
            createDate: new Date(2020, 10, 5),
            title: 'Moto GP',
            description: 'Es la máxima categoría del Campeonato del Mundo de Motociclismo, considerado el certamen internacional más importante en el ámbito de motociclismo de velocidad. Su organización viene determinada por la Federación Internacional de Motociclismo (FIM).',
            image: 'https://images.freeimages.com/images/large-previews/421/honda-600-1466693.jpg'
        }, {
            id: '3',
            createDate: new Date(2020, 10, 12),
            title: 'Baloncesto',
            description: 'Es un deporte de equipo, jugado entre dos conjuntos de cinco jugadores cada uno durante cuatro períodos o cuartos de diez o doce minutos cada uno. El objetivo del equipo es anotar puntos introduciendo un balón por la canasta, un aro a 3,05 metros sobre la superficie de la pista de juego del que cuelga una red.',
            image: 'https://images.freeimages.com/images/large-previews/edc/basketball-in-net-1526540.jpg'
        }, {
            id: '4',
            createDate: new Date(2020, 10, 3),
            title: 'Natación',
            description: 'Es un deporte que consiste en el desplazamiento de una persona en el agua, sin que esta toque el suelo. Es regulado por la Federación Internacional de Natación.',
            image: 'https://images.freeimages.com/images/large-previews/981/swim-1354509.jpg'
        }, {
            id: '5',
            createDate: new Date(2020, 10, 20),
            title: 'Ciclismo',
            description: 'Es un deporte en el que se utiliza una bicicleta para recorrer Circuitos al Aire Libre o en Pista Cubierta.'
        }]
    }
} : state;
