
import {ICard} from '../../model';

import {store} from '../store';
import {Action, Dispatch} from '../types/redux';
import {CREATE_UPDATE_CARD, DELETE_CARD, UPDATE_CURRENT_CARD} from '../types/cardsTypes';


export const createUpdateCard = (card: ICard): Action => {

    return (dispatch: Dispatch) => {

        const state = store.getState();
        let payload: ICard[];

        if(state.card.cards.find(c => c.id === card.id)) {

            payload = state.card.cards.map(c => c.id === card.id ? card : c);

        } else {
            payload = [...state.card.cards , card];
        }

        dispatch({
            type: CREATE_UPDATE_CARD,
            payload
        });
    };
}

export const deleteCard = (id: string): Action => {

    return (dispatch: Dispatch) => {

        const state = store.getState();

        dispatch({
            type: DELETE_CARD,
            payload: state.card.cards.filter(c => c.id !== id)
        });
    };
}

export const updateCurrentCard = (id: string = ''): Action => {

    return (dispatch: Dispatch) => {

        dispatch({
            type: UPDATE_CURRENT_CARD,
            payload: id
        });
    };
}
