
import {Reducer} from 'redux';

import {initialState} from '../initialState';
import {ICardState} from '../IAppState';
import {CREATE_UPDATE_CARD, DELETE_CARD, UPDATE_CURRENT_CARD} from '../types/cardsTypes';


export const cardReducer: Reducer<ICardState> = (state: ICardState = initialState.card, {type, payload}) => {

    switch(type) {

        case CREATE_UPDATE_CARD:
            return {
                ...state,
                cards: payload
            };

        case DELETE_CARD:
            return {
                ...state,
                cards: payload
            };

        case UPDATE_CURRENT_CARD:
            return {
                ...state,
                updateCurrent: payload
            }

        default:
            return state;
    }
}
