
import {combineReducers, Reducer} from 'redux';

import {IAppState} from '../IAppState';
import {cardReducer} from './cardReducer';


export const rootReducer: Reducer<IAppState> = combineReducers({
    card: cardReducer,
});
