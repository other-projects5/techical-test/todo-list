
import {Color} from '@material-ui/lab';


export interface IWarning {

    type: Color | null;

    message: string;

}
