

export interface ICard {

    id: string;

    createDate: Date;

    title: string;

    description: string;

    image?: string;

}
