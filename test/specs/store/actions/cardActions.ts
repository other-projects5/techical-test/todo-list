
import createMockStore, {MockStoreCreator, MockStoreEnhanced} from 'redux-mock-store';
import thunk from 'redux-thunk';
import {v4 as uuidv4} from 'uuid';

import {Dispatch} from '../../../../src/store/types/redux';
import {
    CREATE_UPDATE_CARD,
    DELETE_CARD,
    UPDATE_CURRENT_CARD
} from '../../../../src/store/types/cardsTypes';
import {initialState} from '../../../../src/store/initialState';
import {IAppState} from '../../../../src/store/IAppState';
import {
    createUpdateCard,
    deleteCard,
    updateCurrentCard
} from '../../../../src/store/actions/cardActions';

import {ICard} from '../../../../src/model';


const middlewares: any[] = [thunk];
const mockStore: MockStoreCreator<IAppState, Dispatch> = createMockStore<IAppState, Dispatch>(middlewares);


describe('Action creators - cardActions', () => {

    let store: MockStoreEnhanced<IAppState, Dispatch>;

    beforeEach(() => {
        store = mockStore(initialState);
    });


    test('createUpdateCard - create', () => {

        // Mocks
        const card: ICard = {
            id: uuidv4(),
            createDate: new Date(),
            title: 'Test title',
            description: 'Test description'
        };

        // Simulation
        store.dispatch(createUpdateCard(card));
        const actions = store.getActions();

        // Assertions
        expect(actions[0]).toEqual({
            type: CREATE_UPDATE_CARD,
            payload: [...initialState.card.cards, card]
        });
    });

    test('createUpdateCard - update', () => {

        // Mocks
        const card: ICard = {
            id: '1',
            createDate: new Date(),
            title: 'Test title',
            description: 'Description'
        };

        // Simulation
        store.dispatch(createUpdateCard(card));
        const actions = store.getActions();

        // Assertions
        expect(actions[0]).toEqual({
            type: CREATE_UPDATE_CARD,
            payload: [...initialState.card.cards, card]
        });
    });

    test('deleteCard', () => {

        // Mocks
        const id: string = '1';

        // Simulation
        store.dispatch(deleteCard(id));
        const actions = store.getActions();

        // Assertions
        expect(actions[0]).toEqual({
            type: DELETE_CARD,
            payload: [...initialState.card.cards.filter(c => c.id !== id)]
        });
    });

    test('updateCurrentCard', () => {

        // Mocks
        const id: string = '1';

        // Simulation
        store.dispatch(updateCurrentCard(id));
        const actions = store.getActions();

        // Assertions
        expect(actions[0]).toEqual({
            type: UPDATE_CURRENT_CARD,
            payload: id
        });
    });
});
