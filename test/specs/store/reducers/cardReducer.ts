
import {cardReducer} from '../../../../src/store/reducers/cardReducer';
import {initialState} from '../../../../src/store/initialState';
import {
    CREATE_UPDATE_CARD,
    DELETE_CARD,
    UPDATE_CURRENT_CARD
} from '../../../../src/store/types/cardsTypes';


describe('Reducers - cardReducer', () => {

    const state = initialState.card;

    test('Should return initialState', () => {

        // Mocks
        const actionMock = {
            type: 'invalid-type',
            payload: {}
        };

        // Simulation
        const res = cardReducer(state, actionMock);

        // Assertion
        expect(res).toEqual(state);
    });

    test('create card', () => {

        // Mocks
        const payload = [
            ...state.cards, {
                id: '123',
                createDate: new Date(),
                title: 'qwe',
                description: 'qwe'
            }];

        const actionMock = {
            type: CREATE_UPDATE_CARD,
            payload
        };

        // Simulation
        const res = cardReducer(state, actionMock);

        // Assertion
        expect(res).toEqual({
            ...state,
            cards: payload
        });
    });

    test('update card', () => {

        // Mocks
        const id = '1';
        const payload = state.cards.map(c => c.id === id ? {...c, title: 'Changed'} : c);

        const actionMock = {
            type: CREATE_UPDATE_CARD,
            payload
        };

        // Simulation
        const res = cardReducer(state, actionMock);

        // Assertion
        expect(res).toEqual({
            ...state,
            cards: payload
        });
    });

    test('delete card', () => {

        // Mocks
        const id = '1';
        const payload = state.cards.filter(c => c.id !== id);

        const actionMock = {
            type: DELETE_CARD,
            payload
        };

        // Simulation
        const res = cardReducer(state, actionMock);

        // Assertion
        expect(res).toEqual({
            ...state,
            cards: payload
        });
    });

    test('update current card', () => {

        // Mocks
        const id = '1';

        const actionMock = {
            type: UPDATE_CURRENT_CARD,
            payload: id
        };

        // Simulation
        const res = cardReducer(state, actionMock);

        // Assertion
        expect(res).toEqual({
            ...state,
            updateCurrent: id
        });
    });
});
