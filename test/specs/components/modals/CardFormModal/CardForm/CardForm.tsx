
import * as React from 'react';
import {Provider} from 'react-redux';
import renderer from 'react-test-renderer';

import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {CardForm} from '../../../../../../src/components/modals/CardFormModal/CardForm';
import {createStoryStore} from '../../../../../stories/utils';


configure({adapter: new Adapter()});

describe('CardForm', () => {

    test('Should render CardForm', () => {

        const onSubmitMock = jest.fn();

        const tree = renderer.create(
            <Provider store={createStoryStore()} >
                <CardForm onSubmit={onSubmitMock} />
            </Provider>
        ).toJSON();

        expect(tree).toMatchSnapshot();
    });
});
