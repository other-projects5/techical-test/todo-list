
import * as React from 'react';
import {Provider} from 'react-redux';
import renderer from 'react-test-renderer';

import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {createStoryStore} from '../../../../../stories/utils';
import {Card} from '../../../../../../src/components/partials/CardList/Card';


configure({adapter: new Adapter()});

describe('Card', () => {

    test('Should render Card', () => {

        const tree = renderer.create(
            <Provider store={createStoryStore()} >
                <Card id="1" createDate={new Date()} title="title" description="desc" />
            </Provider>
        ).toJSON();

        expect(tree).toMatchSnapshot();
    });
});
