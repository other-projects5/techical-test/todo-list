
import * as React from 'react';
import {Provider} from 'react-redux';
import renderer from 'react-test-renderer';

import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {createStoryStore} from '../../../../stories/utils';
import {CardList} from '../../../../../src/components/partials';


configure({adapter: new Adapter()});

describe('CardList', () => {

    test('Should render CardList', () => {

        const tree = renderer.create(
            <Provider store={createStoryStore()} >
                <CardList />
            </Provider>
        ).toJSON();

        expect(tree).toMatchSnapshot();
    });
});
