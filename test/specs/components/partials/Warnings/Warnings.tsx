
import * as React from 'react';
import renderer from 'react-test-renderer';

import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {Warnings} from '../../../../../src/components/partials/Warnings';


configure({adapter: new Adapter()});

describe('Warnings', () => {

    test('Should render Warnings', () => {

        const tree = renderer.create(<Warnings type="info" message="Testing component" />).toJSON();
        expect(tree).toMatchSnapshot();
    });

    test('Should NOT render Warnings', () => {

        const tree = renderer.create(<Warnings type={null} message="Testing component" />).toJSON();
        expect(tree).toMatchSnapshot();
    });
});
