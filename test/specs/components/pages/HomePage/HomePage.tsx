
import * as React from 'react';
import {Provider} from 'react-redux';
import renderer from 'react-test-renderer';

import {configure} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {createStoryStore} from '../../../../stories/utils';
import {HomePage} from '../../../../../src/components/pages';


configure({adapter: new Adapter()});

describe('HomePage', () => {

    test('Should render HomePage', () => {

        const tree = renderer.create(
            <Provider store={createStoryStore()} >
                <HomePage />
            </Provider>
        ).toJSON();

        expect(tree).toMatchSnapshot();
    });
});
