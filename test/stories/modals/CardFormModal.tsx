
import * as React from 'react';
import {Provider} from 'react-redux';
import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';

import {CssBaseline} from '@material-ui/core';

import {MuiThemeProvider} from '../../../src/components/MuiThemeProvider';
import {CardFormModal} from '../../../src/components/modals';

import {ComponentWrapper, createStoryStore} from '../utils';


storiesOf('modals/CardForm', module)
    .addDecorator((story) => (
        <MuiThemeProvider>
            <CssBaseline/>
            <Provider store={createStoryStore()} >
                <ComponentWrapper style={{display: 'block'}} >
                    {story()}
                </ComponentWrapper>
            </Provider>
        </MuiThemeProvider>
    ))
    .add('default', () => (
        <CardFormModal visible={true} onClose={action('onClose')} />
    ));
