
import * as React from 'react';
import {Provider} from 'react-redux';
import {storiesOf} from '@storybook/react';

import {CssBaseline} from '@material-ui/core';

import {IAppState} from '../../../src/store/IAppState';
import {initialState} from '../../../src/store/initialState';

import {MuiThemeProvider} from '../../../src/components/MuiThemeProvider';
import {CardList} from '../../../src/components/partials';

import {ComponentWrapper, createStoryStore} from '../utils';


const state: IAppState = {
    card: initialState.card
};

storiesOf('partials/CardList', module)
    .addDecorator((story) => (
        <MuiThemeProvider>
            <CssBaseline/>
            <Provider store={createStoryStore(state)} >
                <ComponentWrapper style={{display: 'block'}} >
                    {story()}
                </ComponentWrapper>
            </Provider>
        </MuiThemeProvider>
    ))
    .add('default', () => (
        <CardList />
    ));
