
import * as React from 'react';
import {Provider} from 'react-redux';
import {storiesOf} from '@storybook/react';

import {CssBaseline} from '@material-ui/core';

import {Card} from '../../../src/components/partials/CardList/Card';
import {MuiThemeProvider} from '../../../src/components/MuiThemeProvider';

import {ComponentWrapper, createStoryStore} from '../utils';


storiesOf('partials/Card', module)
    .addDecorator((story) => (
        <MuiThemeProvider>
            <CssBaseline/>
            <Provider store={createStoryStore()} >
                <ComponentWrapper style={{margin: '0 auto', width: '350px'}} >
                    {story()}
                </ComponentWrapper>
            </Provider>
        </MuiThemeProvider>
    ))
    .add('With image', () => (
        <Card id='1'
              createDate={new Date()}
              title="Con Imagen"
              description="Esta tarjeta tiene el link de una imagen"
              image="https://image.freepik.com/foto-gratis/balon-futbol-linea-blanca-estadio_1150-5285.jpg" />
    ))
    .add('Without image', () => (
        <Card id='1'
              createDate={new Date()}
              title="Sin Imagen"
              description="Esta tarjeta no tiene imagen" />
    ));
