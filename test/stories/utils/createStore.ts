

import {applyMiddleware, createStore, Store} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import {IAppState} from '../../../src/store/IAppState';
import {initialState} from '../../../src/store/initialState';
import {rootReducer} from '../../../src/store/reducers/rootReducer';


const middleware = [thunk];

export function createStoryStore(changes: Partial<IAppState> = {}): Store {

    return createStore(
        rootReducer,
        {
            ...initialState,
            ...changes
        },
        composeWithDevTools(applyMiddleware(...middleware))
    );

}
